import Moveable from './moveable';
import config from './config';
import { sum, minus, isEqual } from './utils/vector2';

const SEGMENTS_COUNT = 3;

export default class Snake extends Moveable {
  constructor(data) {
    super(data);
    this.reset = () => {
      const { x, y } = data.position;
      this.length = SEGMENTS_COUNT;
      let position = [];
      for (let i = 0; i < this.length; ++i) {
        position.push({ x, y: y - i });
      }
      this.newDirection = null;
      this.segments = {
        direction: Array(this.length).fill(data.direction),
        position,
      };
    };
    this.reset();
  }

  render(context) {
    context.fillStyle = 'black';

    this.segments.position.forEach(({ x, y }) => {
      context.fillRect(
        Math.round(x) * config.pixelsForCell,
        Math.round(y) * config.pixelsForCell,
        config.pixelsForCell - 1,
        config.pixelsForCell - 1
      );
    });
  }

  onStep(newPosition) {
    const { position, direction } = this.segments;

    if (this.newDirection) {
      const isOppositeDirection =
        Math.abs(this.newDirection.x) + Math.abs(direction[0].x) === 2 ||
        Math.abs(this.newDirection.y) + Math.abs(direction[0].y) === 2;

      if (!isOppositeDirection) {
        this.segments.direction[0] = this.newDirection;
        this.newDirection = null;
      }
    }

    this.segments.position = position.map((pos, index) => sum(pos, direction[index]));

    for(let i = this.segments.direction.length - 1; i > 0; i--) {
      this.segments.direction[i] = direction[i - 1];
    }

    if (this.checkEdgeCollision() || this.checkBodyCollision()) {
      this.reset();
    }
  }

  setDirection(newDirection) {
    const { x, y } = this.segments.direction[0];
    const isOppositeDirection =
      Math.abs(newDirection.x) + Math.abs(x) === 2 ||
      Math.abs(newDirection.y) + Math.abs(y) === 2;

    if (!isOppositeDirection) {
      this.newDirection = newDirection;
    }
  }

  growth() {
    const { direction, position } = this.segments;
    const lastId = this.length - 1;
    direction.push(direction[lastId]);
    position.push(minus(position[lastId], direction[lastId]));
    this.length++;
  }

  checkBodyCollision() {
    const unCollisionSegments = 3;
    if (this.length <= unCollisionSegments) {
      return;
    }
    const { position } = this.segments;

    for (let i = unCollisionSegments; i < this.length; i++ ) {
      if (isEqual(position[i], this.headPosition)) {
        return true;
      }
    }
    return false;
  }

  checkEdgeCollision() {
    const { x, y } = this.headPosition;

    return  x > config.rowCells - 1 || x < 0 ||
            y > config.columnCells - 1 || y < 0;
  }

  get headPosition() {
    return this.segments.position[0];
  }
}