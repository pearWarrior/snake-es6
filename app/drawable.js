export default class Drawable {
  render(context) {
    throw Error('Must be override.');
  }
}