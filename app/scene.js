import config from './config';
import Snake from './snake';
import Food from './food';
import KEYS from './utils/keys';
import { isEqual } from './utils/vector2';
import { positionToIndex, indexToPosition } from './utils/utils';

const DIRECTIONS = {
  [KEYS.UP]: { x: 0, y: -1 },
  [KEYS.LEFT]: { x: -1, y: 0 },
  [KEYS.DOWN]: { x: 0, y: 1 },
  [KEYS.RIGHT]: { x: 1, y: 0 },
};

const CELLS_COUNT = config.rowCells * config.columnCells;

export default class Scene {
  constructor(context) {
    context.canvas.height = config.rowCells * config.pixelsForCell;
    context.canvas.width = config.columnCells * config.pixelsForCell;

    this.snake = new Snake(config.snakeInit);
    this.food = null;
    this.spawnFood();

    window.addEventListener('keydown', ({ keyCode }) => {
      const newDirection = DIRECTIONS[keyCode];

      if (!newDirection) {
        return;
      }
      this.snake.setDirection(newDirection);
    });

    let time = Date.now();

    const gameLoop = () => {
      const currentTime = Date.now();
      context.clearRect(0, 0, context.canvas.width,  context.canvas.height);
      this.snake.update(currentTime - time);
      if (isEqual(this.snake.headPosition, this.food.position)) {
        this.snake.growth();
        this.spawnFood();
      }
      time = currentTime;
      this.snake.render(context);
      this.food.render(context);
      requestAnimationFrame(gameLoop);
    };
    requestAnimationFrame(gameLoop);
  }

  spawnFood() {
    const excludeIds = this.snake.segments.position.map(pos => positionToIndex(pos));
    const ids = Array.from(Array(CELLS_COUNT).keys()).filter(value => excludeIds.indexOf(value) === -1);
    const randomId =  ids[Math.floor(Math.random() * ids.length)];
    this.food = new Food(indexToPosition(randomId));
  }
}
