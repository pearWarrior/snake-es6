import Drawable from './drawable';
import { isEqual, round } from './utils/vector2';

export default class Moveable extends Drawable {
  constructor({ speed, position, direction }) {
    super();
    this.speed = speed;
    this.currentPosition = position;
    this.direction = direction;
  }

  onStep(newPosition) {
    // Empty
  }

  update(dt) {
    const { x, y } = this.currentPosition;
    const { speed, direction } = this;
    const newPosition = {
      x: x + direction.x * speed * dt,
      y: y + direction.y * speed * dt,
    };

    if (!isEqual(round(this.currentPosition), round(newPosition))) {
      this.onStep(newPosition);
    }
    this.currentPosition = newPosition;
  }
}
