export default {
  rowCells: 20,
  columnCells: 20,
  pixelsForCell: 30,
  snakeInit: {
    position: { x: 4, y: 4 },
    speed: 0.004,
    direction: { x: 0, y: 1 }
  }
};
