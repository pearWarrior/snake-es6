import config from '../config'

export function positionToIndex({ x, y }) {
  return Math.round(y) * config.rowCells +  Math.round(x);
}

export function indexToPosition(id) {
  return { x: id % config.rowCells, y: Math.floor(id / config.rowCells) };
}
