export default {
  UP: 38,
  DOWN: 40,
  RIGHT: 39,
  LEFT: 37,
  SPACE: 32,
}