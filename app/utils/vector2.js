export function isEqual(vec1, { x, y }) {
  return vec1.x === x && vec1.y === y;
}

export function round({ x, y }) {
  return { x: Math.round(x), y: Math.round(y) };
}

export function sum(vec1, { x, y }) {
  return { x: vec1.x + x, y: vec1.y + y }
}

export function minus(vec1, { x, y }) {
  return { x: vec1.x - x, y: vec1.y - y }
}