'use strict';
import './style.scss';
import Scene from './scene';

const canvas = document.getElementById('main__canvas');
const ctx = canvas.getContext('2d');
new Scene(ctx);