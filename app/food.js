import Drawable from './drawable';
import config from './config';

export default class Food extends Drawable {
  constructor(position) {
    super();
    this.position = position;
  }

  render(context) {
    context.fillStyle = 'black';
    const { x, y } = this.position;

    context.fillRect(
      Math.round(x) * config.pixelsForCell,
      Math.round(y) * config.pixelsForCell,
      config.pixelsForCell - 1,
      config.pixelsForCell - 1
    );
  }
}
